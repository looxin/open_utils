
// ----------------------------
// Collection structure for streams
// ----------------------------
db.getCollection("streams").drop();
db.createCollection("streams");

// ----------------------------
// Documents of streams
// ----------------------------
db.getCollection("streams").insert([ {
    _id: ObjectId("000000000000000000000001"),
    "creator_user_id": "local:admin",
    "is_default_stream": true,
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": false,
    description: "Contains messages that are not explicitly routed to other streams",
    "created_at": ISODate("2024-03-21T08:25:32.471Z"),
    disabled: false,
    title: "Default Stream"
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("000000000000000000000002"),
    "creator_user_id": "admin",
    "is_default_stream": false,
    "index_set_id": "65fbeefd63166c4279024a8d",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "Stream containing all events created by Graylog",
    "created_at": ISODate("2024-03-21T08:25:33.865Z"),
    disabled: false,
    title: "All events"
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("000000000000000000000003"),
    "creator_user_id": "admin",
    "is_default_stream": false,
    "index_set_id": "65fbeefd63166c4279024a8e",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "Stream containing all system events created by Graylog",
    "created_at": ISODate("2024-03-21T08:25:33.871Z"),
    disabled: false,
    title: "All system events"
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a386"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "",
    "created_at": ISODate("2024-03-25T01:32:27.214Z"),
    disabled: false,
    title: "xMail - Test",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a38b"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "",
    "created_at": ISODate("2024-03-25T01:32:27.223Z"),
    disabled: false,
    title: "HUAWEI - USER",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3a4"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "",
    "created_at": ISODate("2024-03-25T01:32:27.784Z"),
    disabled: false,
    title: "NetFlow - Internet",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3a9"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "OR",
    "remove_matches_from_default_stream": true,
    description: "策略命中日志",
    "created_at": ISODate("2024-03-25T01:32:27.788Z"),
    disabled: false,
    title: "HUAWEI - POLICYPERMIT",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3ba"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "",
    "created_at": ISODate("2024-03-25T01:32:27.847Z"),
    disabled: false,
    title: "HUAWEI - WLAN_LOG_USER",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3be"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "DHCP分配日志",
    "created_at": ISODate("2024-03-25T01:32:27.852Z"),
    disabled: false,
    title: "DDI - DHCP分配日志",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3cb"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "OR",
    "remove_matches_from_default_stream": true,
    description: "",
    "created_at": ISODate("2024-03-25T01:32:27.893Z"),
    disabled: false,
    title: "NetFlow - 内网",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3d2"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "流量日志",
    "created_at": ISODate("2024-03-25T01:32:27.903Z"),
    disabled: false,
    title: "HUAWEI - Traffic",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3d9"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "威胁日志",
    "created_at": ISODate("2024-03-25T01:32:27.925Z"),
    disabled: false,
    title: "HUAWEI - ASSOC",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3e6"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "命令记录",
    "created_at": ISODate("2024-03-25T01:32:27.981Z"),
    disabled: false,
    title: "HUAWEI - CMDRECORD",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42b63166c427904a3eb"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "",
    "created_at": ISODate("2024-03-25T01:32:27.996Z"),
    disabled: false,
    title: "Default - xMail",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42c63166c427904a3ff"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "",
    "created_at": ISODate("2024-03-25T01:32:28.055Z"),
    disabled: false,
    title: "xMail",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42c63166c427904a403"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "会话日志",
    "created_at": ISODate("2024-03-25T01:32:28.059Z"),
    disabled: false,
    title: "HUAWEI - SECLOG",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42c63166c427904a419"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "OR",
    "remove_matches_from_default_stream": true,
    description: "历史告警",
    "created_at": ISODate("2024-03-25T01:32:28.241Z"),
    disabled: false,
    title: "AlarmHistory",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42c63166c427904a422"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "URL日志",
    "created_at": ISODate("2024-03-25T01:32:28.264Z"),
    disabled: false,
    title: "HUAWEI - URL",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42c63166c427904a438"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "STA/5/IWAP_LOG_CLIENT_INFO_ROAM_OR_OFFLINE_INFO",
    "created_at": ISODate("2024-03-25T01:32:28.304Z"),
    disabled: false,
    title: "HUAWEI - IWAP_LOG_CLIENT_INFO_ROAM_OR_OFFLINE_INFO",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42c63166c427904a43c"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "DHCP交互日志",
    "created_at": ISODate("2024-03-25T01:32:28.307Z"),
    disabled: false,
    title: "DDI - DHCP交互日志",
    "content_pack": null,
    outputs: [ ]
} ]);
db.getCollection("streams").insert([ {
    _id: ObjectId("6600d42c63166c427904a446"),
    "creator_user_id": "admin",
    "index_set_id": "65fbeefc63166c42790249d6",
    "matching_type": "AND",
    "remove_matches_from_default_stream": true,
    description: "",
    "created_at": ISODate("2024-03-25T01:32:28.35Z"),
    disabled: false,
    title: "HUAWEI - BLACKLIST",
    "content_pack": null,
    outputs: [ ]
} ]);


// ----------------------------
// Collection structure for index_sets
// ----------------------------
db.getCollection("index_sets").drop();
db.createCollection("index_sets");
db.getCollection("index_sets").createIndex({
    "index_prefix": NumberInt("1")
}, {
    name: "index_prefix_1",
    unique: true
});
db.getCollection("index_sets").createIndex({
    "creation_date": NumberInt("-1")
}, {
    name: "creation_date_-1"
});

// ----------------------------
// Documents of index_sets
// ----------------------------
db.getCollection("index_sets").insert([ {
    _id: ObjectId("65fbeefc63166c42790249d6"),
    title: "Default index set",
    description: "The Graylog default index set",
    "index_prefix": "graylog",
    shards: NumberInt("1"),
    replicas: NumberInt("0"),
    "rotation_strategy_class": "org.graylog2.indexer.rotation.strategies.TimeBasedSizeOptimizingStrategy",
    "rotation_strategy": {
        type: "org.graylog2.indexer.rotation.strategies.TimeBasedSizeOptimizingStrategyConfig",
        "index_lifetime_min": "P30D",
        "index_lifetime_max": "P40D"
    },
    "retention_strategy_class": "org.graylog2.indexer.retention.strategies.DeletionRetentionStrategy",
    "retention_strategy": {
        type: "org.graylog2.indexer.retention.strategies.DeletionRetentionStrategyConfig",
        "max_number_of_indices": NumberInt("20")
    },
    "creation_date": ISODate("2024-03-21T08:25:32.428Z"),
    "index_analyzer": "standard",
    "index_template_name": "graylog-internal",
    "index_template_type": null,
    "index_optimization_max_num_segments": NumberInt("1"),
    "index_optimization_disabled": false,
    "field_type_refresh_interval": NumberLong("5000"),
    "custom_field_mappings": [ ],
    writable: true,
    regular: true
} ]);
db.getCollection("index_sets").insert([ {
    _id: ObjectId("65fbeefd63166c4279024a8d"),
    title: "Graylog Events",
    description: "Stores Graylog events.",
    "index_prefix": "gl-events",
    shards: NumberInt("1"),
    replicas: NumberInt("0"),
    "rotation_strategy_class": "org.graylog2.indexer.rotation.strategies.TimeBasedSizeOptimizingStrategy",
    "rotation_strategy": {
        type: "org.graylog2.indexer.rotation.strategies.TimeBasedSizeOptimizingStrategyConfig",
        "index_lifetime_min": "P30D",
        "index_lifetime_max": "P40D"
    },
    "retention_strategy_class": "org.graylog2.indexer.retention.strategies.DeletionRetentionStrategy",
    "retention_strategy": {
        type: "org.graylog2.indexer.retention.strategies.DeletionRetentionStrategyConfig",
        "max_number_of_indices": NumberInt("20")
    },
    "creation_date": ISODate("2024-03-21T08:25:33.861Z"),
    "index_analyzer": "standard",
    "index_template_name": "gl-events-template",
    "index_template_type": "events",
    "index_optimization_max_num_segments": NumberInt("1"),
    "index_optimization_disabled": false,
    "field_type_refresh_interval": NumberLong("5000"),
    "custom_field_mappings": [ ],
    writable: true,
    regular: false
} ]);
db.getCollection("index_sets").insert([ {
    _id: ObjectId("65fbeefd63166c4279024a8e"),
    title: "Graylog System Events",
    description: "Stores Graylog system events.",
    "index_prefix": "gl-system-events",
    shards: NumberInt("1"),
    replicas: NumberInt("0"),
    "rotation_strategy_class": "org.graylog2.indexer.rotation.strategies.TimeBasedSizeOptimizingStrategy",
    "rotation_strategy": {
        type: "org.graylog2.indexer.rotation.strategies.TimeBasedSizeOptimizingStrategyConfig",
        "index_lifetime_min": "P30D",
        "index_lifetime_max": "P40D"
    },
    "retention_strategy_class": "org.graylog2.indexer.retention.strategies.DeletionRetentionStrategy",
    "retention_strategy": {
        type: "org.graylog2.indexer.retention.strategies.DeletionRetentionStrategyConfig",
        "max_number_of_indices": NumberInt("20")
    },
    "creation_date": ISODate("2024-03-21T08:25:33.868Z"),
    "index_analyzer": "standard",
    "index_template_name": "gl-system-events-template",
    "index_template_type": "events",
    "index_optimization_max_num_segments": NumberInt("1"),
    "index_optimization_disabled": false,
    "field_type_refresh_interval": NumberLong("5000"),
    "custom_field_mappings": [ ],
    writable: true,
    regular: false
} ]);
